##
# go-bf
#
# @file
# @version 0.1

.PHONY: run build clean

run: build
	./bf hello.bf

build:
	go build -o bf main.go

clean:
	rm -rf bf

# end
