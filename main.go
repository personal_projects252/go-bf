package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

var (
	ErrBrackets = errors.New("incorrect brackets")
)

type bfstate struct {
	memory [30000]byte
	pc int
	ip int
}

// NewState returns Brainfuck state with PC set to the middle of memory (tape).
func NewState() *bfstate {
	return &bfstate{
		pc: 30000/2,
	}
}

func main() {
	if len(os.Args) != 2 {
		fmt.Fprintf(os.Stderr, "usage: %s <code.bf>\n", os.Args[0])
		return
	}

	code, err := cleanCode(os.Args[1])
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s", err)
		return
	}
	if err = verifyCode(code); err != nil {
		fmt.Fprintf(os.Stderr, "%s", err)
		return
	}

	state := NewState()

	process(state, code, nil)
}

// isAllowedChar checks if given char is in Brainfuck charset.
func isAllowedChar(char rune) bool {
	charset := "+-<>,.[]"
	return strings.ContainsRune(charset, char)
}

// cleanCode returns code that contains only chars from Brainfuck charset.
func cleanCode(path string) ([]byte, error) {
	path, err := filepath.Abs(os.Args[1])
	if err != nil {
		return nil, err
	}

	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	reader := bufio.NewReader(f)

	code := make([]byte, 0, 10)
	for {
		char, _, err := reader.ReadRune()
		if err != nil {
			break
		}
		if isAllowedChar(char) {
			code = append(code, byte(char))
		}
	}

	return code, nil
}

// verifyCode checks brackets placement correctness.
func verifyCode(code []byte) error {
	stack := make([]int, 0, 2)
	for i, char := range code {
		switch char {
		case '[':
			stack = append(stack, i)
		case ']':
			if len(stack) == 0 {
				return ErrBrackets
			}
			stack = stack[:len(stack)-1]
		default:
			continue
		}
	}

	if len(stack) != 0 {
		return ErrBrackets
	}
	return nil
}

// process executes given code with given input.
func process(state *bfstate, code []byte, input []byte) error {
	loops := matchBrackets(code)
	var i int
	for state.ip != len(code) {
		switch code[state.ip] {
		case '+':
			state.memory[state.pc] += 1
		case '-':
			state.memory[state.pc] -= 1
		case '<':
			state.pc -= 1
		case '>':
			state.pc += 1
		case ',':
			if input != nil && i < len(input) {
				state.memory[state.pc] = input[i]
				i += 1
			}
		case '.':
			fmt.Print(string(state.memory[state.pc]))
		case '[':
			if state.memory[state.pc] == 0 {
				state.ip = loops[state.ip]
			}
		case ']':
			if state.memory[state.pc] != 0 {
				state.ip = loops[state.ip]
			}
		}
		state.ip += 1
	}
	return nil
}

// matchBrackets return map of openning and corresponding closing brackets.
func matchBrackets(code []byte) map[int]int {
	loops := make(map[int]int)
	stack := make([]int, 0, 2)
	for i, char := range code {
		switch char {
		case '[':
			stack = append(stack, i)
		case ']':
			openPos := stack[len(stack)-1]
			stack = stack[:len(stack)-1]
			loops[openPos] = i
			loops[i] = openPos
		default:
			continue
		}
	}
	return loops
}
